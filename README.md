# !ARCHIVED!

This project has been archived, along with all other POP and Idem-based projects.
- For more details: [Salt Project Blog - POP and Idem Projects Will Soon be Archived](https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/)

# ci-templates

gitlab-ci templates

# docker

kaniko builder templates

# lint

pre-commit

# test

test jobs for windows, macos, linux

# build

build jobs for windows, macos, linux

# upload

upload job

# release

release job
